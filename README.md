# Node.js Microservice Template

This is a coding template that demontrates our prefered way of structuring a node.js microservice. It includes topics such as folder structure, file/module types and usage etc

The structure is based on the following design decisions:
* Keep the architecture as pragmatic and simple as possible. Avoid unneccesary layers and abstractions. Stay away from the object oriented dogm.
* All microservices shall communicate with other services using event messaging (pubilsh/subscribe)
* Some microservices will not even offer an API for sychronous requests
* The service business logic shall be completely unaware of the API (no http status codes etc)
* The service business logic shall mainly reside (or be orchistrated) inside the service.js file inte /src root folder.
* Models shall be plain data structures only (no methods). Use schemas for validation.
* The persistence will predominantely be made using NoSql json documents.

The example used in this template is a microservcice for processing loan applications from customers, including receiving offers from different banks. The example is of course far from complete.

Here is an overview of the internal software architecure:

<img src="architecture-overview.png" alt="Software Architecture Overview" width="600px" >

## 1. Folders and Files

```
.
└───src
│   │
│   └───api
│   │   └───controllers
|   |       loan-application-controller.js
|   |   routes.js
|   |
│   └───database
│   │   loan-application-repository.js
│   │
│   └───integrations
|   |   customers-client.js
│   │   
│   └───schemas
|   |   customer-schema.js
|   |   loan-application-schema.js
|   |   loan-offer-schema.js
|   |
│   service.js
│
└───test
│   loan-application-api-tests.js
|   loan-application-service-tests.js
|
└───database
│   │
│   └───repositories
│   │   loan-application-repository.js
│   db.js
|
server.js

```
