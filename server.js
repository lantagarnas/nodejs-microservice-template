require('dotenv').config()

//Set up injection of mock modules 
if (process.env.MOCK) {
    const mock = require('mock-require');
    mock('./src/fbg/fbg-pubsub', './src/fbg/fbg-pubsub-mock');
    mock('./src/database/loan-application-repository', './src/database/mock/loan-application-repository');
}

//Import modules
const express = require('express')
const app = express()
const helmet = require('helmet')
const cors = require('cors')
const bodyParser = require('body-parser')
const routes = require('./src/api/routes')
const loanApplicationService = require('./src/service')

//Prepare for app start
const port = typeof process.env.APP_PORT === 'undefined' ? 8080 : process.env.APP_PORT
app.use(helmet())
app.use(helmet.noCache())
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//Set up the routes
app.use('/', routes)

//Start the server
app.listen(port, () => {
    console.log(`Server listening on port ${port}.`)
    if (process.env.MOCK) console.log('Using MOCK data and processing.')
    loanApplicationService.startListeningToBusinessEvents()
    console.log('\n')
})

//Set up grecful shutdown
process.on('SIGTERM', gracefulShutdown);
process.on('SIGINT', gracefulShutdown);

function gracefulShutdown() {
    console.log('\nShutting down server.')
    loanApplicationService.stopListeningToBusinessEvents()
    process.exit(0);
}

//Export app for testing
module.exports = app
