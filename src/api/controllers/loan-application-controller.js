const loanApplicationsService = require('../../service')
const {sendServiceResult, sendBadRequest, sendInternalServerError} = require('../../fbg/fbg-http-response')
const {Failure} = require('../../fbg/fbg-service-results')
const {isUUID, isObject, hasProperty} = require('../../fbg/fbg-validation')

async function show(req, res, next) {
    try {
        if (!isUUID(req.params.id)) return sendBadRequest(res, 'id', 'Parameter id must be a vaild UUID.')

        const id = req.params.id
        const result = await loanApplicationsService.getById(id);
        return sendServiceResult(res, result)
    } catch (error) {
        return next(error)
    }
}

async function create(req, res, next) {
    try {
        if (!isObject(req.body)) return sendBadRequest(res, 'body', 'The request body must have properties.')
        if (!hasProperty(req.body, 'amount')) return sendBadRequest(res, 'amount', 'Missing property: amount.')
        if (!hasProperty(req.body, 'paybackTime')) return sendBadRequest(res, 'paybackTime', 'Missing property: paybackTime.')

        const result = await loanApplicationsService.receiveLoanApplicationFromCustomer(req.body)
        return sendServiceResult(res, result)
    } catch (error) {
        return next(error)
    }
}

module.exports.show = show
module.exports.create = create
