const express = require('express')
const router = express.Router()
const loanApplicationController = require('./controllers/loan-application-controller')

router.get('/loan-applications/:id', loanApplicationController.show)
router.post('/loan-applications', loanApplicationController.create)

module.exports = router
