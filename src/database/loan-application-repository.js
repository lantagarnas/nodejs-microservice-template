// @ts-nocheck
const { query } = require('../fbg/fbg-postgres-client')

async function getById(id) {
    let loanApplication = null
    const result = await query("SELECT * FROM loan_applications WHERE id = $1", [id])
    if (result && result.rowCount > 0) { 
        loanApplication = result.rows[0].data
    }
    return loanApplication
}

async function store(loanApplication) {
    const result = await query("INSERT INTO loan_applications (id, data) VALUES ($1, $2) ON CONFLICT (id) DO UPDATE SET data = $2", [loanApplication.id, loanApplication])
    if (result instanceof Error) 
        return false
    else
        return true
}

module.exports.getById = getById
module.exports.store = store