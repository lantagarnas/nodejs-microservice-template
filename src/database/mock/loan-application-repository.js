const uuid = require('uuid/v1')

let loanApplications = seedData()

function seedData() {
    let applications = []
    applications.push({
        id:'68930ae5-ea6d-471b-b038-553162076870', 
        amount: 100000, 
        paybackTime: 18,
        offers : []
     })

     applications[0].offers.push({
         id: uuid(),
         amount: 1000000, 
         paybackTime: 18,
         interest: 7.5
     })

     applications[0].offers.push({
        id: uuid(),
        amount: 80000, 
        paybackTime: 18,
        interest: 6.5
    })
    applications[0].offers.push({
        id: uuid(),
        amount: 100000, 
        paybackTime: 24,
        interest: 9.5
    })
    
    applications.push({
        id:'1748d73a-cf76-4a8d-ab69-fcdc79e7b973', 
        amount: 150000, 
        paybackTime: 12,
        offers : []
     })

     applications.push({
        id:'68930ae5-ea6d-471b-b038-553162076870', 
        amount: 200000, 
        paybackTime: 24,
        offers : []
     })
    return applications
}

async function all() {
    return loanApplications
}

async function getById(id) {
    let application = null
    for (let i = 0; i < loanApplications.length; i++) {
        if (loanApplications[i].id === id) {
            application = loanApplications[i]
            break
        }
    }
    return application
}

async function store(loanApplication) {
    loanApplications.push(loanApplication)
    return true
}

module.exports.all = all
module.exports.getById = getById
module.exports.store = store