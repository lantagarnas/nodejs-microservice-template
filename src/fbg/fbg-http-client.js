const axios = require('axios').default;

const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DEL = 'DELETE';

class HTTPClient {

	static source

	static set host(h) { this._host = h }
	static get host() { return this._host }

	static set token(t) { this._token = t }
	static get token() { return this._token }

    static async call(method, path, payload, headers = {}) {

        const CancelToken = axios.CancelToken
        HTTPClient.source = CancelToken.source()

        const url = this._host + path
        headers = {
            "Authorization": "Bearer " + this._token,
            "User-Agent": "FBG",
            ...headers
        }

        switch (method) {
            case GET:
                return await axios.get(url, {
                    headers: headers,
                    cancelToken: this.source.token
                });
            case POST:
                return await axios.post(url, payload, {
                    headers: headers,
                    cancelToken: this.source.token
                });
            case PUT:
                return await axios.put(url, payload, {
                    headers: headers,
                    cancelToken: this.source.token
                });
            case DEL:
                return await axios.delete(url, {
                    headers: headers,
                    cancelToken: this.source.token
                });
            default:
                console.log("Client: not a valid method: " + method);
        }
    }

    static async get(url)               { return await this.call(GET, url); }
    static async post(url, payload)     { return await this.call(POST, url, payload); }
    static async put(url, payload)      { return await this.call(PUT, url, payload); }
    static async patch(url, payload)    { return await this.call(PUT, url, payload); }
    static async delete(url)            { return await this.call(DEL, url); }

    static async cancel() {
        await HTTPClient.source.cancel('Operation canceled by the user.');
    }
}

module.exports = HTTPClient
