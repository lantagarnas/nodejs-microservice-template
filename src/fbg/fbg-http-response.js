function sendServiceResult(res, result) {
    let status, body

    if (result.status === 'success') {
        status = 200
        body = bodyContent('success', result.data)
    }

    else if (result.status === 'failure' && result.data.type === 'notfound-failure') {
        status = 404;
        body = bodyContent('failure', { errors: [{ reference: result.data.value, message: 'Not found.' }] })
    }

    else if (result.status === 'failure') {
        status = 422;
        body = bodyContent('failure', { errors: buildErrorCollection(result) })
    }

    return res.status(status).send(body)
}

function sendBadRequest(res, reference, message) {
    const status = 400
    const body = bodyContent('failure', { errors: [{ reference: reference, message: message }] })
    return res.status(status).send(body)
}

function sendInternalServerError(res, reference, message) {
    const status = 500
    const body = bodyContent('failure', { errors: [{ reference: status, message: 'Internal Server Error' }] })
    return res.status(status).send(body)
}

function buildErrorCollection(result) {
    const errors = []
    if (!result.data.errors || !Array.isArray(result.data.errors)) return errors
    for (let i = 0; i < result.data.errors.length; i++) {
        const error = result.data.errors[i]
        errors.push({ reference: error.property, message: error.message })
    }
    return errors
}

function bodyContent(status, data) {
    return {
        status: status,
        data: data
    }
}

module.exports.sendServiceResult = sendServiceResult
module.exports.sendBadRequest = sendBadRequest
module.exports.sendInternalServerError = sendInternalServerError
