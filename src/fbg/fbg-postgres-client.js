require('dotenv').config()
const { Pool } = require('pg')

const isProduction = process.env.NODE_ENV === 'production'

const pool = new Pool({
	user: process.env.SQL_USER,
    password: process.env.SQL_PASSWORD,
    database: process.env.SQL_DATABASE,
	host: isProduction ? `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}` : 'localhost'
})

pool.on('error', (err, client) => {
	console.error('POSTGRES: Unexpected error on idle client', err)
	process.exit(-1)
})

async function query(text, params) {
	try {
		return await pool.query(text, params)
	} catch (error) {
		// TODO: Real error handling
		return new Error(error.message)
	}
}

module.exports.pool = pool
module.exports.query = query

