const { PubSub } = require('@google-cloud/pubsub')
const pubsub = new PubSub()
//const timeout = 30

function listen(subscriptionName, callback) {
	const subscription = pubsub.subscription(subscriptionName);
	subscription.on('message', (message) => { messageHandler(message, callback, subscriptionName) });
}

const messageHandler = async function (message, callback, subscriptionName = null) {
	try {
		const subscription = subscriptionName.split('/').slice(-1)[0]
		console.log(`Received message [${message.id}]: <${subscription}> ${message.data}`)
		const result = await callback(message.data)
		if (result.isFailure) {
			//const publishSuccess = publish({result:result, message: message}, 'dead-letter-topic')
			console.log({result:result, message: message})
		}
		message.ack();
	} catch (error) {
		// TODO: Real error handling
		console.log('ERROR: Message handler failed.')
		console.log(error)
		process.exit(1)
	}
};

function stopListening(subscriptionName) {
	const subscription = pubsub.subscription(subscriptionName);
	subscription.removeListener('message', messageHandler);
}

async function publish(data, topicName) {
	try {
		const pubsub = new PubSub();
		const jsonString = JSON.stringify(data)
		const dataBuffer = Buffer.from(jsonString);
		const messageId = await pubsub.topic(topicName).publish(dataBuffer);
		return true
	} catch (error) {
		//TODO: Real error handling
		console.log('ERROR: Publish failed.')
		return null
	}
}

module.exports.listen = listen
module.exports.stopListening = stopListening
module.exports.publish = publish
