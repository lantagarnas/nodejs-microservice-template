const serviceName = process.env.SERVICE_NAME || ''

class Result {
    constructor(status) {
        this.status = status
        this.service = serviceName
        this.data = {}
    }
    get isSuccess() { return this.status === 'success' ? true : false}
    get isFailure() { return !this.isSuccess}
}

class Success extends Result {
    constructor(data) {
        super('success')
        this.data = data
    }
}

class Failure extends Result {
    constructor(description) {
        super('failure')
        this.data.type = 'general-failure'
        this.data.description = description || ''
    }
}

class ValidationFailure extends Failure {
    constructor(errors, description) {
        super()
        this.data.type = 'validation-failure'
        this.data.description = description || ''
        if (Array.isArray(errors))
            this.data.errors = errors
        else if (typeof errors === 'string')
            this.data.errors = [errors]
    }
}

class IntegrationFailure extends Failure {
    constructor(integration, key, description) {
        super()
        this.data.type = 'integration-failure'
        this.data.description = description || ''
        this.data.integration = integration || ''
        this.data.key = key || ''
    }
}

class NotFoundFailure extends Failure {
    constructor(obj, key, value, description) {
        super()
        this.data.type = 'notfound-failure'
        this.data.description = description || ''
        this.data.object = obj || ''
        this.data.key = key || ''
        this.data.value = value || ''
    }
}

class PersistenceFailure extends Failure {
    constructor(key, value, description) {
        super()
        this.data.description = description || ''
        this.data.key = key || ''
        this.data.value = value || ''
    }
}

class PubSubFailure extends Failure {
    constructor(topic, key, description) {
        super()
        this.data.description = description || ''
        this.data.topic = topic || ''
        this.data.key = key || ''
    }
}

module.exports.Success = Success
module.exports.Failure = Failure
module.exports.ValidationFailure = ValidationFailure
module.exports.IntegrationFailure = IntegrationFailure
module.exports.NotFoundFailure = NotFoundFailure
module.exports.PersistenceFailure = PersistenceFailure
module.exports.PubSubFailure = PubSubFailure
