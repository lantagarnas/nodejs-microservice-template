const Ajv = require('ajv');

 function validateSchema(data, schema, requiredFields = true) {
    const ajv = new Ajv({ allErrors: true })
    const errors = []
    if (!requiredFields) {
        delete schema.required
    }
    const valid = ajv.validate(schema, data)

    if (!valid) {
        for (let i = 0; i < ajv.errors.length; i++) {
            const ajvError = ajv.errors[i]
            const error = {
                property: (ajvError.dataPath || ''),
                message: (ajvError.message || '')
            }
            errors.push(error)
        }
        return errors
    }
    return null
}


function isUUID(uuid) {
    const ajv = new Ajv({ allErrors: true })
    const isValid = ajv.validate({
        "properties": {
            "id": {
                "type": ["string"],
                "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$",
                "description": "uuid"
            }
        }
    }, { id: uuid })
    return isValid
}

function isString(str) {
    return (typeof str === 'string')
}

function isStringOfLength(str, length) {
    return (typeof str === 'string' && str.length >= length)
}

function isBetween(num, min, max) {
    return (typeof (num) === 'number' && num >= min && num <= max)
}

function hasProperty(obj, prop) {
    return (obj.hasOwnProperty(prop))
}

function hasProperties(obj, properties) {
    for (let i = 0; i < properties.length; i++) {
        if (!obj.hasOwnProperty(properties[i])) {
            return false
        }
    }
    return true
}

function isObject(obj) {
    return Object.keys(obj).length > 0 && obj.constructor === Object
}

module.exports.validateSchema = validateSchema
module.exports.isUUID = isUUID
module.exports.isString = isString
module.exports.isStringOfLength = isStringOfLength
module.exports.isBetween = isBetween
module.exports.hasProperty = hasProperty
module.exports.hasProperties = hasProperties
module.exports.isObject = isObject
