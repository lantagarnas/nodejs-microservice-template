const HTTPClient = require('../fbg/fbg-http-client')

async function getCustomer(customerId) {
    HTTPClient.host = process.env.CUSTOMERS_API_HOST
    HTTPClient.token = process.env.CUSTOMERS_API_TOKEN
    //const result = await HTTPClient.get('/customers/' + customerId)
    //MOCK:
    const result = {}
    result.data = {ssn : '710325-1638', phone: '0702-686499' }
    return result.data
}

module.exports.getCustomer = getCustomer
