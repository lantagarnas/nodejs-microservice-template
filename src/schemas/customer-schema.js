module.exports = {
    required: ['ssn', 'phone'],
    properties: {
        ssn: {
            type: ['string'],
            description: 'social security number'
        },
        phone: {
            type: ['string'],
            description: 'phone number'
        },
    }
}