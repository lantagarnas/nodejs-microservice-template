require('dotenv').config()
const LoanOffer = require('./loan-offer-schema')

const MIN_LOAN_AMOUNT = process.env.LOAN_MIN_LOAN_AMOUNT;
const MAX_LOAN_AMOUNT = process.env.LOAN_MAX_LOAN_AMOUNT;
const MIN_PAYBACK_TIME = process.env.LOAN_MIN_PAYBACK_TIME;
const MAX_PAYBACK_TIME = process.env.LOAN_MAX_PAYBACK_TIME;

module.exports =  {
    required: ['id', 'amount', 'paybackTime'],
    properties: {
        amount: {
            type: 'integer',
            description: 'positive integer',
            minimum: parseInt(MIN_LOAN_AMOUNT),
            maximum: parseInt(MAX_LOAN_AMOUNT)
        },
        paybackTime: { 
            type: 'integer',
            description: 'number of months',
            minimum: parseInt(MIN_PAYBACK_TIME),
            maximum: parseInt(MAX_PAYBACK_TIME)
        },
        offers: {
            items: LoanOffer.properties
        }
    }
}

