const uuid = require('uuid/v1')
const customerService = require('./integrations/customers-client')
const loanApplicationSchema = require('./schemas/loan-application-schema')
const customerSchema = require('./schemas/customer-schema')
const loanOfferSchema = require('./schemas/loan-offer-schema')
const validateSchema  = require('./fbg/fbg-validation').validateSchema
const { Success, ValidationFailure, IntegrationFailure, PersistenceFailure, PubSubFailure, NotFoundFailure  } = require('./fbg/fbg-service-results')
const pubsub = require('./fbg/fbg-pubsub')
const LoanApplicationRepository = require('./database/loan-application-repository')
require('dotenv').config()

const loanOfferCreatedSubscription = process.env.PUBSUB_LOAN_OFFER_CREATED_SUBSCRIPTION
const loanApplicationCreatedTopic = process.env.PUBSUB_LOAN_APPLICATION_CREATED_TOPIC
const entityName = 'Loan Application'

async function getById(id) {
    let loanApplication = await LoanApplicationRepository.getById(id)
    if (!loanApplication) return new NotFoundFailure(entityName, 'id', id)
    return new Success(loanApplication)
}

async function receiveLoanApplicationFromCustomer(loanApplication) {

    // Create a new LoanApplication
    loanApplication.id = uuid()

    // Validate the loan application data
    let errors = validateSchema(loanApplication, loanApplicationSchema)
    if (errors) return new ValidationFailure(errors, entityName)

    //Get customer details (call the customer service api synchronously)
    const customer = await customerService.getCustomer(loanApplication.customerId)
    if (!customer) return new IntegrationFailure('getCustomer', loanApplication.customerId, 'Customers Service')
    errors = validateSchema(customer, customerSchema)
    if (errors) return new ValidationFailure(errors, 'Customer')

    // Persist the loan application to the db
    const saveSuccess = await LoanApplicationRepository.store(loanApplication)
    if (!saveSuccess) return new PersistenceFailure('id', loanApplication.id, entityName)

    // Publish the data as a business event
    const publishSuccess = await pubsub.publish(loanApplication, loanApplicationCreatedTopic)
    if (!publishSuccess) return new PubSubFailure(loanApplicationCreatedTopic, loanApplication.id)

    return new Success(loanApplication)
}

async function receiveLoanOffer(loanOffer) {
    console.log('Loan offer received: ' + loanOffer)
}

function startListeningToBusinessEvents() {
    pubsub.listen(loanOfferCreatedSubscription, receiveLoanOffer)
    console.log('Loan Application Service is listening to business events.')
}

function stopListeningToBusinessEvents() {
    pubsub.stopListening(loanOfferCreatedSubscription)
    console.log('Loan Application Service stopped listening to business events.')
}

module.exports.getById = getById
module.exports.receiveLoanApplicationFromCustomer = receiveLoanApplicationFromCustomer
module.exports.receiveLoanOffer = receiveLoanOffer
module.exports.startListeningToBusinessEvents = startListeningToBusinessEvents
module.exports.stopListeningToBusinessEvents = stopListeningToBusinessEvents
