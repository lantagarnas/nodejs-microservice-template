const a = require('../src/fbg/fbg-pubsub')
const mock = require('mock-require');
mock('../src/fbg/fbg-pubsub', '../src/fbg/fbg-pubsub-mock');
mock('../src/database/repositories/loan-application-repository', '../src/database/repositories/mock/loan-application-repository');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const assert = require('assert');
const should = chai.should();

chai.use(chaiHttp);

describe('API Tests', async function () {

    describe('GET /loan-applications/68930ae5-ea6d-471b-b038-553162076870', () => {
        it('it should successfully GET 1 specific loan application', (done) => {
            chai.request(server)
                .get('/loan-applications/68930ae5-ea6d-471b-b038-553162076870')
                .end((err, res) => {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.data.id, '68930ae5-ea6d-471b-b038-553162076870')
                    done();
                });
        });
    });
    describe('POST /loan-applications', () => {
        it('it should successfully POST and create a vaild loan application', (done) => {
            const vaildLoanApplication = {
                amount : 200000,
                paybackTime: 12
            }
            chai.request(server)
                .post('/loan-applications')
                .send(vaildLoanApplication)
                .end((err, res) => {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.status, 'success')
                    assert.equal(res.body.data.id.length, 36)
                    done();
                });
        });
        it('it should fail to POST an invaild loan application', (done) => {
            const invalidLoanApplication = {
                amount : 20000000, //Too high
                paybackTime: 0 //Too low
            }
            chai.request(server)
                .post('/loan-applications')
                .send(invalidLoanApplication)
                .end((err, res) => {
                    assert.equal(res.status, 422)
                    assert.equal(res.body.status, 'failure')
                    assert.notEqual(res.body.data.type, 'validation-failure')
                    assert.notEqual(res.body.data.description, 'Loan Application')
                    assert.equal(res.body.data.errors.length, 2)
                    done();
                });
        });
    });
});
