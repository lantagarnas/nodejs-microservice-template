const mock = require('mock-require');
mock('../src/fbg/fbg-pubsub', '../src/fbg/fbg-pubsub-mock');
mock('../src/database/repositories/loan-application-repository', '../src/database/repositories/mock/loan-application-repository');

const assert = require('assert');
const loanApplicationService = require('../src/service')

describe('Loan Application Service Tests', async function () {

    describe('#getById', async function () {
        it('should sucessfully get 1 application', async function () {
            const validId = '68930ae5-ea6d-471b-b038-553162076870'
            const result = await loanApplicationService.getById(validId)
            assert.equal(result.status, 'success')
            assert.equal(result.data.id, validId)
            assert.equal(result.isSuccess, true)
            assert.equal(result.isFailure, false)
            assert.equal(result.service, 'Loan Application Service')
        });
    });

    describe('#receiveCustomerApplication', async function () {
        it('should sucessfully receive and create a new application', async function () {
            const data = {
                amount: 200000,
                paybackTime: 12
            }
            const result = await loanApplicationService.receiveLoanApplicationFromCustomer(data)
            assert.equal(result.status, 'success');
            assert.equal(result.data.id.length, 36)
        });
        it('should refuse to accept an invalid application (amount too high)', async function () {
            const data = {
                amount: 20000000, //too high!
                paybackTime: 0 //too low!
            }
            const result = await loanApplicationService.receiveLoanApplicationFromCustomer(data)
            assert.equal(result.status, 'failure');
            assert.equal(result.data.type, 'validation-failure')
            assert.equal(result.data.description, 'Loan Application')
            assert.equal(result.data.errors.length, 2)
        });
    });
});
